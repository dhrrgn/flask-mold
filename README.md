# Flask Boilerplate Application

A simple boilerplate Flask application.

## Installation

#### Clone the Repository

    $ git clone https://github.com/dandoescode/flask-mold.git my_app
    $ cd my_app

#### Configure

Modify the settings in `mold/settings.py`

#### Create the Schema

    $ python manage.py createschema

#### Create a User

    $ python manage.py createuser

#### Run the Server

    $ python manage.py runserver

