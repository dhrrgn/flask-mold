from flask.ext.script import Manager

from mold.frontend import create_app
from mold.manage import CreateSchemaCommand, CreateUserCommand, RunServerCommand

manager = Manager(create_app())
manager.add_command('createschema', CreateSchemaCommand())
manager.add_command('createuser', CreateUserCommand())
manager.add_command('runserver', RunServerCommand())

if __name__ == "__main__":
    manager.run()