from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.seasurf import SeaSurf
from .exts import init_db

db = SQLAlchemy()
init_db(db)

csrf = SeaSurf()

lm = LoginManager()
