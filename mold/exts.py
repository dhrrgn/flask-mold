from flask.ext.wtf import HiddenField
from flask import g, current_app


def setup_form(form_class):
    csrf_name = current_app.config.get('CSRF_COOKIE_NAME', '_csrf_token')
    setattr(form_class, csrf_name, HiddenField(default=getattr(g, csrf_name)))


def init_db(db):
    def save(model):
        db.session.add(model)
        db.session.commit()

    db.Model.save = save
