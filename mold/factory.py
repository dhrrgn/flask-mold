from flask import Flask

from .core import db, lm, csrf
from .helpers import register_blueprints


def create_app(package_name, package_path=None, settings=None):
    app = Flask(package_name, instance_relative_config=True)

    app.config.from_object('mold.settings')
    app.config.from_object(settings)

    db.init_app(app)
    csrf.init_app(app)
    lm.init_app(app)

    if package_path is not None:
        register_blueprints(app, package_name, package_path)

    return app
