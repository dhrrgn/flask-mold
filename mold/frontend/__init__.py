from functools import wraps

from flask import render_template, g, current_app
from flask.ext.login import current_user

from .. import factory, core
from . import assets
from ..users.models import User


def create_app(settings=None):
    app = factory.create_app(__name__, __path__, settings)

    app.before_request(user_setup)
    assets.init_app(app)
    core.lm.user_loader(User.load_user)
    core.lm.login_view = "users.login"

    app.errorhandler(404)(error_404)
    app.errorhandler(403)(error_403)
    app.errorhandler(500)(error_500)

    return app


def route(bp, *args, **kwargs):
    """Wraps the route() call on the given Blueprint

    We wrap the call here so that we can hook in and grab the response easily for debugging
    as well as add logging.
    """
    def decorator(func):
        current_route = args[0]

        @bp.route(*args, **kwargs)
        @wraps(func)
        def wrapper(*args, **kwargs):
            if current_app.debug:
                print "Executing {0}.{1} for '{2}'".format(func.__module__, func.__name__, current_route)
            return func(*args, **kwargs)
        return wrapper

    return decorator


def user_setup():
    g.current_user = current_user
    g.logged_in = False
    if current_user.is_authenticated() and not current_user.is_anonymous():
        g.logged_in = True


def error_404(e):
    return render_template('errors/404.html'), 404


def error_403(e):
    return render_template('errors/403.html'), 403


def error_500(e):
    return render_template('errors/500.html'), 500
