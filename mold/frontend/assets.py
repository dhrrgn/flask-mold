from flask.ext.assets import Environment, Bundle


def init_app(app):
    assets = Environment(app)

    css = Bundle('css/bootstrap.css', 'css/style.css',
                 filters=['yui_css'],
                 output="{0}/mold.css".format(app.config.get('asset_path', 'gen')))
    js_head = Bundle('js/libs/modernizr-2.6.2.min.js',
                     filters=['closure_js'],
                     output="{0}/pre.js".format(app.config.get('asset_path', 'gen')))
    js_body = Bundle('js/main.js',
                     filters=['closure_js'],
                     output="{0}/app.js".format(app.config.get('asset_path', 'gen')))

    assets.register('css_all', css)
    assets.register('js_head', js_head)
    assets.register('js_body', js_body)

    assets.manifest = 'cache' if not app.debug else False
    assets.cache = not app.debug
    assets.debug = app.debug