from flask import Blueprint, render_template, redirect, request, url_for, flash, g
from flask.ext.login import login_required, login_user, logout_user, login_url
from ..users.models import User
from ..users.forms import LoginForm, PasswordForm, ProfileForm
from ..exts import setup_form
from . import route

bp = Blueprint('users', __name__)


@route(bp, '/login', methods=['POST', 'GET'])
def login():
    setup_form(LoginForm)
    form = LoginForm()

    if form.validate_on_submit():
        u = User.attempt_login(form.email.data, form.password.data)
        if u is not None:
            login_user(u)
            return redirect(request.args.get("next") or url_for('frontend.index'))
        else:
            flash(u'Email or password incorrect', 'error')

    url = login_url("users.login", request.args.get("next"))
    return render_template('login.html', login_form=form, login_url=url)


@route(bp, '/logout')
@login_required
def logout():
    logout_user()
    flash(u'Successfully logged out', 'success')
    return redirect(url_for('frontend.index'))


@route(bp, '/profile', methods=['POST', 'GET'])
@login_required
def profile():
    setup_form(PasswordForm)
    setup_form(ProfileForm)
    profile_form = ProfileForm()
    password_form = PasswordForm()

    if not profile_form.is_submitted():
        profile_form.name.data = g.current_user.name
        profile_form.email.data = g.current_user.email
    elif profile_form.validate_on_submit():
        g.current_user.email = profile_form.email.data
        g.current_user.name = profile_form.name.data
        g.current_user.save()

        flash(u'Successfully Updated Profile', 'success')

    return render_template('edit_profile.html', profile_form=profile_form, password_form=password_form)


@route(bp, '/change_password', methods=['POST', 'GET'])
@login_required
def change_password():
    profile_form = ProfileForm()
    password_form = PasswordForm()

    profile_form.name.data = g.current_user.name
    profile_form.email.data = g.current_user.email

    if password_form.validate_on_submit():
        if g.current_user.change_password(password_form.current_password.data, password_form.new_password.data):
            g.current_user.save()
            flash(u'Successfully Changed Password', 'success')
        else:
            flash(u'Current Password entered was Invalid', 'error')

    return render_template('edit_profile.html', profile_form=profile_form, password_form=password_form)


