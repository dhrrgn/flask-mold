from flask.ext.script import Command

from ..core import db


class CreateSchemaCommand(Command):
    """Create the database schema."""

    def run(self):
        print('Creating Tables...')
        db.create_all()
