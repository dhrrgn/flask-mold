from flask.ext.script import Command

from wsgi import runserver


class RunServerCommand(Command):
    """Runs the server."""

    def run(self):
        runserver()
