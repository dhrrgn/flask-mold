from flask.ext.script import Command, prompt, prompt_pass

from ..models import *


class CreateUserCommand(Command):
    """Create a new user."""

    def run(self):
        name = prompt('Name')
        email = prompt('Email')
        password = prompt_pass('Password')
        password_confirm = prompt_pass('Confirm Password')

        if password != password_confirm:
            print 'Error: Passwords do not match!'
            return

        u = User(email=email, password=password, name=name)
        u.save()

        print 'User Created!'
