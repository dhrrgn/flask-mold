import os

DEBUG = False
SECRET_KEY = 'S3cR3t_K3y_4_u!?'

if 'DATABASE_URL' in os.environ:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
else:
    SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost/foobar'

ASSET_PATH = 'gen'

# This is disabling the Flask-WTF CSRF
CSRF_ENABLED = False

# This is for SeaSurf
CSRF_COOKIE_NAME = 'csrf_token'
