from flask.ext.wtf import DataRequired, TextField, PasswordField, Email, EqualTo, Form


class LoginForm(Form):
    """ The WTF Login Form
    """
    email = TextField('email', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired()])


class ProfileForm(Form):
    """ The WTF Profile Form
    """
    name = TextField('name', validators=[DataRequired()])
    email = TextField('email', validators=[DataRequired(), Email()])


class PasswordForm(Form):
    """ The WTF Profile Form
    """
    new_password = PasswordField('new_password')
    confirm_new_password = PasswordField('confirm_new_password',
                                         validators=[EqualTo('new_password', message='Passwords do not match')])
    current_password = PasswordField('current_password')