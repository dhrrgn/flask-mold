from ..core import db
from hashlib import md5
from passlib.hash import bcrypt


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    name = db.Column(db.String(128), nullable=False)

    @staticmethod
    def load_user(user_id):
        return User.query.get(user_id)

    @staticmethod
    def attempt_login(email, password):
        u = User.query.filter_by(email=email).first()
        if u and bcrypt.verify(password, u.password):
            return u

        return None

    def __init__(self, email, password, name):
        self.email = email
        self.password = bcrypt.encrypt(password)
        self.name = name

    def __repr__(self):
        return '<User %s>' % self.email

    def change_password(self, current_password, new_password):
        if bcrypt.hashpw(current_password, self.password) != self.password:
            return False

        self.password = bcrypt.hashpw(new_password, bcrypt.gensalt(12))
        return True

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def avatar(self, size=64):
        return 'https://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + '?d=mm&s=' + str(size)
