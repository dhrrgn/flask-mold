Flask
Flask-Assets
Flask-Login
flask-seasurf
Flask-WTF
Flask-Script
Flask-SQLAlchemy
mysql-python
# psycopg2
passlib
py-bcrypt

# Asset Stuff
closure
yuicompressor

# Server Stuffs
werkzeug
gunicorn
wsgiref
