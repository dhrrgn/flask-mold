from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

from mold import frontend

app = DispatcherMiddleware(frontend.create_app(), {
    # Other Mounts
})


def runserver():
    run_simple('0.0.0.0', 5000, app, use_reloader=True, use_debugger=True)


if __name__ == "__main__":
    runserver()
